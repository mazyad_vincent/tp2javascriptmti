const winning = (board, player) => {
    if (
        (board[0] === player && board[1] === player && board[2] === player) ||
        (board[3] == player && board[4] === player && board[5] === player) ||
        (board[6] === player && board[7] === player && board[8] === player) ||
        (board[0] === player && board[3] === player && board[6] === player) ||
        (board[1] === player && board[4] === player && board[7] === player) ||
        (board[2] === player && board[5] === player && board[8] === player) ||
        (board[0] === player && board[4] === player && board[8] === player) ||
        (board[2] === player && board[4] === player && board[6] === player)
        ) 
    {
        return true;
    } 
    else {
        return false;
    }
}

const get_available_spot = (map) => {
    let available_array = []
    map.forEach((element, index) => {
            if (element === null)
                available_array.push(index)
    });
    return available_array
}

// current player == 'x' if player, 'o' if ia
const get_ia_pos = (map, currentPlayer) => {

    
    const available_spot  = get_available_spot(map)
    if (winning(map, 'x'))   
        return {score: 10}
    else if (winning(map, 'o'))
        return {score: -10}
    else if (available_spot.length === 0)
        return {score : 0}
    let moves = []
    for (let i = 0; i < available_spot.length; ++i)
    {
        let new_map = [...map]
        console.log(available_spot)
        let move = {};
        move.index = available_spot[i]
        new_map[move.index] = currentPlayer
        let result = (currentPlayer === 'x')? get_ia_pos(new_map, 'o') : get_ia_pos(new_map, 'x')
        move.score = result.score
        moves.push(move)
    }
    
    let bestMove;
    if (currentPlayer === 'x'){
        let bestScore = -10000
        for (let i = 0; i < moves.length; ++i)
        {
            if (moves[i].score > bestScore)
            {
                bestScore = moves[i].score
                bestMove = i
            }
        }
    }

    else {
        var bestScore = 10000
        for (let i = 0; i < moves.length; ++i)
        {
            if (moves[i].score < bestScore)
            {
                bestScore = moves[i].score
                bestMove = i
            }
        }
    }
    return moves[bestMove]
}

export const minmax = (map) => {
    return get_ia_pos(map, 'o').index
}
