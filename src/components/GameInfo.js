import React from "react";

// FIXME: change message and color based on `gameState`'s value
const GameInfo = (props) => {
  return (
    (!props.gameFinished)? <h3>It's your turn {props.currentPlayer}</h3> : <h3>{props.playerWon} has won</h3>
    )
};

export default GameInfo;
