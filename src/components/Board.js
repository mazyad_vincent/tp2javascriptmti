import React from "react";
import Cell from "./Cell";

const boardStyle = {
  display: "grid",
  width: "600px",
  height: "calc(100%)",
  grid: "auto-flow dense / 1fr 1fr 1fr",
  gridAutoRows: "auto"
};

const Board = ({ cells, currentPlayer, handleClickCell, cellsWon, gameFinished }) => (
  <div style={boardStyle}>{cells.map((c, id) => <Cell cell={c} currentPlayer={currentPlayer} handleClickCell={() => handleClickCell(id)} gameFinished={gameFinished} cellWon={(cellsWon.includes(id)? true : false)} />)}</div>
);

export default Board;
