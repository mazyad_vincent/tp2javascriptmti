import React from "react";

const cellStyleMouseOut = {
  display: "block",
  backgroundColor: "white",
  width: "200px",
  height: "200px",
  border: "1px solid #333",
  outline: "none",
  textAlign: "center",
  lineHeight: "200px",
  cursor: "pointer"
};

const cellStyleWon = {
  display: "block",
  backgroundColor: "green",
  width: "200px",
  height: "200px",
  border: "1px solid #333",
  outline: "none",
  textAlign: "center",
  lineHeight: "200px",
  cursor: "pointer"
};

const cellStyleMouseOver = {
  display: "block",
  backgroundColor: "red",
  width: "200px",
  height: "200px",
  border: "1px solid #333",
  outline: "none",
  textAlign: "center",
  lineHeight: "200px",
  cursor: "pointer"
};

class Cell extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      style : cellStyleMouseOut,
    }
  }

  render(){
    const cell_display = this.props.cell // if the player1 already clicked on this cell, cell_display = x, o otherwise, null if noone clicked on it
    const handleClickEvent = this.props.handleClickCell
    const gameFinished = this.props.gameFinished // did the game finish
    const cellWon = this.props.cellWon // if true, means that a player managed to win using this cell, add a new style if true

    return (
      <div style={(cellWon)? cellStyleWon : this.state.style}
      onMouseOver={() => { if (!gameFinished)
                            this.setState({style : cellStyleMouseOver})}}
      onMouseOut={() => { if (!gameFinished)
                            this.setState({style : cellStyleMouseOut})}}
      onClick={() => {
          if (cell_display == null)
             handleClickEvent()}}>
        {cell_display}
      </div>
    )
  }
}

export default Cell;
