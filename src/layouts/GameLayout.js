import React from "react";
import Board from "../components/Board";
import GameInfo from "../components/GameInfo";
import {minmax} from "../ia/ia"

const gameLayoutStyle = {
  width: 650,
  height: `calc(90%)`,
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  margin: "auto"
};

const buttonStyle = {
  marginTop: 5,
  textAlign: "center",
}

const isGameDone = ({cells}) =>{
  for (let i = 0; i < 3 ; ++i) // check all columns
  {
    if (cells[i] != null && (cells[i] === cells[i + 3] && cells[i] === cells[i + 6]))
      return {isGameFinished : true, playerWon : cells[i], index : [i, i + 3, i + 6]}
  }

  for (let i = 0; i <= 6; i += 3) // check all lines
  {
    if (cells[i] != null && cells[i] === cells[i + 1] && cells[i] === cells[i + 2])
      return {isGameFinished : true, playerWon : cells[i], index : [i, i + 1, i + 2]}
  }

  // check all diago
  if (cells[0] != null && cells[0] === cells[4] && cells[0] === cells[8])
      return {isGameFinished : true, playerWon : cells[0], index : [0, 4, 8]}

  if (cells[2] != null && cells[2] === cells[4] && cells[2] === cells[6])
      return {isGameFinished : true, playerWon : cells[2], index : [2, 4, 6]}
  return {isGameFinished : false}    
}

class GameLayout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cells: Array(9).fill(null),
      currentPlayer: "player 1",
      playerWon : undefined,
      gameFinished : false,
      cellsWon: [] // the id of Cells in array which won the game
    };
  }

  // getDerivedStateFromProps is called before every render,
  // use it to infer new state values from props or state changes.
  static getDerivedStateFromProps(props, state) {
    let is_game_finished = isGameDone(state)
    return (!is_game_finished.isGameFinished)? null :  {
      gameFinished : true,
      playerWon : (is_game_finished.playerWon === 'x')? 'player 1' : 'player2',
      cellsWon : is_game_finished.index
    }; // null if game has not changed
  }
  
  handleClickCell(index)
  {
      if (this.state.gameFinished || this.state.cells[index] !== null)
        return;
      const copy_cell = [...this.state.cells]; // copy the old cell
      copy_cell[index] = this.state.currentPlayer === "player 1" ? 'x' : 'o';
      this.setState({
          cells: copy_cell,
          currentPlayer: this.state.currentPlayer === "player 1"? "player 2" : "player 1"
        }, () => { // ia turn
            if (this.state.gameFinished)
                return
            const index = minmax(this.state.cells) // get the index of ia
            console.log(index)
            let new_cells = [...this.state.cells]
            new_cells[index] = 'o'
            this.setState({
              cells: new_cells,
              currentPlayer: "player 1"
            })
        }
      )
  };

  render() {
    return (
      <div
        style={gameLayoutStyle}>
        <GameInfo currentPlayer={this.state.currentPlayer} gameFinished={this.state.gameFinished} playerWon={this.state.playerWon}/>
        <Board cells={this.state.cells} currentPlayer={this.state.currentPlayer} handleClickCell={(index) => this.handleClickCell(index)} gameFinished={this.state.gameFinished} cellsWon={this.state.cellsWon}/>
        <div style={buttonStyle}>
          <button onClick={() => this.setState({cells: Array(9).fill(null),
                                                currentPlayer: "player 1",
                                                playerWon : undefined,
                                                gameFinished : false,
                                                cellsWon: []})}> 
              Play Again
          </button>
        </div>
      </div>
    );
  }
}

export default GameLayout;
